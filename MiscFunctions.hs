module MiscFunctions 
(   charTimes
    , getISODateTimeNow
    , getISOFromUTCTime
) where

import System.Locale
import Data.Time
import Data.Time.LocalTime
import qualified Data.Map as Map

charTimes :: Char -> String -> Integer
charTimes _ [] = 0
charTimes char string = let
    countList           = Map.toList $ Map.fromListWith (+) [(c,1) | c <- string]
    charLookup          = lookup char countList
    charCount cLookup   = case cLookup of 
                            Just v -> v
                            Nothing -> 0
    in charCount charLookup

getISODateTimeNow :: IO String
getISODateTimeNow = do
    currentDate <- getCurrentTime
    let date = formatTime defaultTimeLocale "%FT%T%Q" currentDate
    return date

getISOFromUTCTime :: UTCTime -> String
getISOFromUTCTime = formatTime defaultTimeLocale "%FT%T%Q"