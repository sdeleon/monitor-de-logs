#!/bin/bash
# Inicializa el script para aplicar saldos a clientes

SCRIPT="aplibot.py"
SCRIPTDIR="/home/recargak/local_scripts/aplibot"
LOGDIR="/home/recargak/local_scripts/aplibot/logs"

USER="aplicador"

DATESTAMP=$(date +%Y-%m-%d)
LOGFILE=${USER}"."${DATESTAMP}".log"

# Comprobar si posee python el sistema
type python >/dev/null 2>&1 || { echo >&2 `date`" No se encontró python en el sistema." >> $LOGDIR/$LOGFILE; exit 1; }

python $SCRIPTDIR/$SCRIPT >> $LOGDIR/$LOGFILE 2>&1