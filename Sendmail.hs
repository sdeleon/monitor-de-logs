{-# LANGUAGE OverloadedStrings #-}

module Sendmail 
(  mailSender
) where

import qualified Data.Text as T
import qualified Data.Text.Lazy as T'
import qualified Data.Configurator as Conf
import qualified Data.Configurator.Types as Types
import System.IO
import Network.Mail.SMTP
import Network.Socket

mailSender :: (String, String, String, String) -> IO()
mailSender ( mailFrom, mailTo, mailSubject, htmlText ) = do
   cfg <- Conf.load [Types.Required "monitor.cnf"]
   email_host <- Conf.require cfg "smtp_login.host" :: IO String
   email_port <- Conf.require cfg "smtp_login.port" :: IO Int
   email_user <- Conf.require cfg "smtp_login.user" :: IO String
   email_pass <- Conf.require cfg "smtp_login.pass" :: IO String

   let port = (fromIntegral email_port) :: PortNumber 
   sendMailWithLogin' email_host port email_user email_pass mail

   where
      from       = Address Nothing (T.pack mailFrom)
      to         = [Address Nothing (T.pack mailTo)]
      cc         = []
      bcc        = []
      subject    = (T.pack mailSubject)
      body       = plainTextPart " "
      html       = htmlPart (T'.pack htmlText)
      mail = simpleMail from to cc bcc subject [body, html]