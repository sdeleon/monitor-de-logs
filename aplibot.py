# encoding=utf-8
"""
Requerimientos:
	Paquetes:
		python-mysqldb
	Librerías integradas:
		time
		gc
		datetime
		smtplib
		email
	En la base de datos:
		Índice de dos columnas en tabla Depósitos constituyendo [status, id_banco]

Tendrá un horario de funcionamiento.
Se ejecutará mediante cronjobs cada 3 minutos
"""

import gc
import smtplib
from email.mime.text import MIMEText
import time
import credentials

import MySQLdb



####################################################
# Inicialización de variables, no editar.

gc.collect() # Garbage collector
brincar = False
msjError = ""

####################################################
# Configuración Inicial

#Identificador de este bot (para la tabla botlog de estadísticas)
BOT_ID = 1

# Indicador de Versión:
#	1 = Meridian
portalVersion = 1

# ID de usuario de Aplibot en el Sistema
aplibotId = "4"

####################################################
# Conexión a la base de datos

try:
	db = MySQLdb.connect(
		host=credentials.DB_HOST,
		user=credentials.DB_USER,
		passwd=credentials.DB_PASSWORD,
		db=credentials.DB_DATABASE_NAME)
	cursor = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
except MySQLdb.Error, e:
	print time.ctime(), ": Error en la conexión con la base de datos:", e
	cursor.close()
	db.close()
	raise SystemExit

####################################################
# Función de conexión al servidor de mail

# http://stackoverflow.com/questions/984526/correct-way-of-handling-exceptions-in-python
def send_message(addr, to, msg):
	u""" Enviar eMail usando servidor local SMTP sin clave

	addr	- Remitente
	to	- Destinatario (Una dirección si es String, o varias si es una Lista)
	msg	- Objeto tipo MIMEText, debe de tener al menos los objetos de lista: Subject, To y From

	"""
	## Connect to host
	try:
		server = smtplib.SMTP('localhost') #can throw an exception
	except smtplib.socket.gaierror:
		print time.ctime(), ": Falló al determinar el servicio SMTP"
		return False

	## Send message
	try:
		server.sendmail(addr, to, msg.as_string())
		return True
	except: # try to avoid catching Exception unless you have too
		print time.ctime(), ": Falló al mandar el aviso por mail"
		return False
	finally:
		server.quit()

####################################################
# Obtención de los bancos a revisar

subqueryBancos = ""
query = "SELECT permitidos FROM depousuarios WHERE id_usuario = %s AND permitidos > ''" % aplibotId
try:
	cursor.execute(query)
except MySQLdb.Error, e:
	print time.ctime(), ": Error en query para obtener lista de bancos", e
	cursor.close()
	db.close()
	raise SystemExit

resultado = cursor.fetchall()
if len(resultado) != 0:
	# Creación de array de bancos seleccionados
	"""
	for bancos in resultado:
		subqueryBancos += "%s, " % bancos['id_banco']
	subqueryBancos = subqueryBancos[:-2]
	"""
	subqueryBancos = resultado[0]['permitidos'][:-1]
else:
	brincar = True
	msjError += " No hay bancos registrados."

####################################################
# Obtención de depositos y Aplicación de saldo

if not brincar:

	# ----------------------- Función ------------------------
	# Meridian v1
	def operacionesDeposito1( deposito, saldosUsuariosInterno):
		u""" Ejecutar cambios sobre el Depósito, Usuario y Superiores en version1

		Función aplicable para el sistema Meridian v1

			-Devuelve False en caso de que haya algún fallo
			-Devuelve un par de diccionarios en caso de que se ejecute correctamente
				aplicar = Diccionario con los saldos a aplicar sobre un usuario determinado
				mail = Diccionario con el mail que ha de mandar al usuario que depositó
				saldosUsuariosInterno = Diccionario con los saldos de cada usuario actualizado

		"""
		# Definiciones
		portalAbrv = { 0: "rec", 1: "pla" }[int(deposito['tipo'])]
		portalId = int(deposito['tipo'])
		tipoSaldo = { 0: "recargas", 1: "servicios" }[int(deposito['servicio'])]
		depositado = float(deposito['cantidad'])
		idDeposito = int(deposito['id_deposito'])
		portal = deposito['portal']
		razonCliente = deposito['razon']
		folio = deposito['folio']
		remitente = {"rec": "depositos@recargaki.com", "pla": "depositos@planetaemx.com"}[portalAbrv]
		destinatario = deposito['email']
		fechaDep = deposito['fecha'].strftime("%Y-%m-%d %H:%M:%S")
		fechaAlta = deposito['fechaalta'].strftime("%Y-%m-%d %H:%M:%S")
		fechaAplicado = time.strftime("%Y-%m-%d %H:%M:%S")
		portalTitulo = {'rec': 'mirecargatelcel.mx', 'pla': 'larecarga.mx'}[portalAbrv]
		portalSubjectName = ''
		nombreUsuario = deposito['nombreUsuario']
		nombreBanco = deposito['nombreBanco']
		columnSaldo = {'recargas': 'saldo', 'servicios': 'saldo_srv'}[tipoSaldo]

		# Conseguir cadena de comando y comisiones
		# En los resultados el usuario inicial es user0,
		# y los números siguientes son usuarios de mayor nivel [de = Número más chico]
		idUsuario = int(deposito['id_usuario'])
		saldoUsuario = {}
		saldoUsuario['saldo'] = float(saldosUsuariosInterno[idUsuario]['saldo'])
		saldoUsuario['saldo_srv'] = float(saldosUsuariosInterno[idUsuario]['saldo_srv'])
		query = """SELECT
			   table0.id_usuario AS user0,
			   table0.tipo AS nivel0,
			   table0.comision AS comision0,
			   table0.de AS padre0,
			   '{0}' AS saldo0,
			   '{1}' AS saldo_srv0,

			   (CASE WHEN table1.id_usuario IS NULL THEN 0 ELSE table1.id_usuario END ) AS user1,
			   (CASE WHEN table1.tipo IS NULL THEN 0 ELSE table1.tipo END ) AS nivel1,
			   (CASE WHEN table1.comision IS NULL THEN 0 ELSE table1.comision END) AS comision1,
			   (CASE WHEN table1.de IS NULL THEN 0 ELSE table1.de END) AS padre1,
			   (CASE WHEN saldo1.saldo IS NULL THEN 0 ELSE saldo1.saldo END) AS saldo1,
			   (CASE WHEN saldo1.saldo_srv IS NULL THEN 0 ELSE saldo1.saldo_srv END) AS saldo_srv1,

			   (CASE WHEN table2.id_usuario IS NULL THEN 0 ELSE table2.id_usuario END ) AS user2,
			   (CASE WHEN table2.tipo IS NULL THEN 0 ELSE table2.tipo END ) AS nivel2,
			   (CASE WHEN table2.comision IS NULL THEN 0 ELSE table2.comision END) AS comision2,
			   (CASE WHEN table2.de IS NULL THEN 0 ELSE table2.de END) AS padre2,
			   (CASE WHEN saldo2.saldo IS NULL THEN 0 ELSE saldo2.saldo END) AS saldo2,
			   (CASE WHEN saldo2.saldo_srv IS NULL THEN 0 ELSE saldo2.saldo_srv END) AS saldo_srv2,

			   (CASE WHEN table3.id_usuario IS NULL THEN 0 ELSE table3.id_usuario END ) AS user3,
			   (CASE WHEN table3.tipo IS NULL THEN 0 ELSE table3.tipo END ) AS nivel3,
			   (CASE WHEN table3.comision IS NULL THEN 0 ELSE table3.comision END) AS comision3,
			   (CASE WHEN table3.de IS NULL THEN 0 ELSE table3.de END) AS padre3,
			   (CASE WHEN saldo3.saldo IS NULL THEN 0 ELSE saldo3.saldo END) AS saldo3,
			   (CASE WHEN saldo3.saldo_srv IS NULL THEN 0 ELSE saldo3.saldo_srv END) AS saldo_srv3,

			   (CASE WHEN table4.id_usuario IS NULL THEN 0 ELSE table4.id_usuario END ) AS user4,
			   (CASE WHEN table4.tipo IS NULL THEN 0 ELSE table4.tipo END ) AS nivel4,
			   (CASE WHEN table4.comision IS NULL THEN 0 ELSE table4.comision END) AS comision4,
			   (CASE WHEN table4.de IS NULL THEN 0 ELSE table4.de END) AS padre4,
			   (CASE WHEN saldo4.saldo IS NULL THEN 0 ELSE saldo4.saldo END) AS saldo4,
			   (CASE WHEN saldo4.saldo_srv IS NULL THEN 0 ELSE saldo4.saldo_srv END) AS saldo_srv4

			   FROM usuarios AS table0
			   LEFT JOIN usuarios AS table1 ON table1.id_usuario = table0.de AND table1.tipo != 1 AND table1.eliminado = 0
			   LEFT JOIN saldos AS saldo1 ON saldo1.id_usuario = table1.id_usuario
			   LEFT JOIN usuarios AS table2 ON table2.id_usuario = table1.de AND table2.tipo != 1 AND table2.eliminado = 0
			   LEFT JOIN saldos AS saldo2 ON saldo2.id_usuario = table2.id_usuario
			   LEFT JOIN usuarios AS table3 ON table3.id_usuario = table2.de AND table3.tipo != 1 AND table3.eliminado = 0
			   LEFT JOIN saldos AS saldo3 ON saldo3.id_usuario = table3.id_usuario
			   LEFT JOIN usuarios AS table4 ON table4.id_usuario = table3.de AND table4.tipo != 1 AND table4.eliminado = 0
			   LEFT JOIN saldos AS saldo4 ON saldo4.id_usuario = table4.id_usuario

			   WHERE table0.id_usuario = '{2}'

			   FOR UPDATE;"""
		query = query.format(saldoUsuario['saldo'], saldoUsuario['saldo_srv'], idUsuario)

		try:
			cursor.execute(query)
		except MySQLdb.Error, e:
			print time.ctime(), ": Error en query obtener cadena de mando - Folio:", folio, e
			cursor.execute("ROLLBACK")
			return False

		cadenaComando = cursor.fetchall()
		if len(cadenaComando) != 0:
			# Normalización [Usabilidad] Separar el resultado en 5 diferentes entradas en un array
			# Orden por nivel, siempre primero el más bajo
			comisiones = []
			for i in range(5):
				# Revisa si el saldo del usuario o superior ya existe en la lista de saldos
				idUser = int(cadenaComando[0]['user%s' % i])
				if saldosUsuariosInterno.has_key(idUser):
					# Ya existe, usa ese saldo
					tempSaldo = saldosUsuariosInterno[idUser]['saldo']
					tempSaldoSRV= saldosUsuariosInterno[idUser]['saldo_srv']
				else:
					# No existe, utiliza el que acaba de sacar de la base de datos
					tempSaldo = cadenaComando[0]['saldo%s' % i]
					tempSaldoSRV = cadenaComando[0]['saldo_srv%s' % i]
				comisiones.append({
				'user': int(idUser),
				'nivel': int(cadenaComando[0]['nivel%s' % i]),
				'comision': float(cadenaComando[0]['comision%s' % i]),
				'padre': int(cadenaComando[0]['padre%s' % i]),
				'saldo': float(tempSaldo),
				'saldo_srv': float(tempSaldoSRV) })
		else:
			print time.ctime(), ": No se encontraron comisiones del usuario, se trata de un usuario eliminado? - Folio:", folio, e
			cursor.execute("ROLLBACK")
			return False

		# Aplicación de saldo y comisiones para cada nivel de usuario
		primero = True
		esServicio = True if tipoSaldo == "servicios" else False
		nivelDos = ''	# Al final de las iteraciones siempre debe de poseer el id del Usuario Nivel2 de la cadena de comando
		comisionNivelDos = 0
		for i in range(5):
			# Mientras no encuentre lineas = 0, realizará la aplicación de saldo al usuario y cada superior de él
			if comisiones[i]['user'] != 0:
				idUsuarioAplicar = comisiones[i]['user']
				# Salvar los datos del saldo si son nuevos
				if not saldosUsuariosInterno.has_key(idUsuarioAplicar):
					saldosUsuariosInterno[idUsuarioAplicar] = {}
					saldosUsuariosInterno[idUsuarioAplicar]['saldo'] = comisiones[i]['saldo']
					saldosUsuariosInterno[idUsuarioAplicar]['saldo_srv'] = comisiones[i]['saldo_srv']

				saldoUsuarioAplicar = float(comisiones[i][columnSaldo])
				# Estas variables al final de las iteraciones quedarán con los valores del NivelDos
				nivelDos = comisiones[i]['user'] if not esServicio else 0
				comisionNivelDos = comisiones[i]['comision'] if not esServicio else 0
				saldoNivelDos = saldoUsuarioAplicar if not esServicio else 0

				# Inicio de Operaciones y Aplicaciones de saldo
				if primero:
					# Cálculos para el usuario del depósito
					padreInmediato_1 = comisiones[i]['padre']
					comisionPesos_1 = round( depositado * ( comisiones[i]['comision'] / 100 ), 2) if not esServicio else 0
					aplicar_1 = round( comisionPesos_1 + depositado, 2)
					nuevoSaldo = round( aplicar_1 + saldoUsuarioAplicar, 2)
					saldo_1 = saldoUsuarioAplicar
					nuevo_saldo_1 = nuevoSaldo
					primero = False
				else:
					# Código a Ejecutar cuando se están usando los valores de los superiores del usuario que depositó
					comisionPreviousUser = comisiones[i-1]['comision']
					comisionActiva = comisiones[i]['comision'] - comisionPreviousUser
					comisionPesos = round( depositado * (comisionActiva/100), 2)
					nuevoSaldo = round( comisionPesos + saldoUsuarioAplicar, 2)

					# Registrar traspaso
					query = """
					INSERT
					INTO htraspasos (de, para, monto, fecha, comentarios, ip,
					comision, id_deposito, finalu, inicialu, finals, inicials, m, tipo)
					VALUES ('1', '%s', '%s', '%s', '', '127.0.0.1', '%s', '%s', '%s', '%s', '0', '0', '0', '0')
					""" % (idUsuarioAplicar, comisionPesos, fechaAplicado,
						comisionPesos, idDeposito, nuevoSaldo, saldoUsuarioAplicar)
					try:
						cursor.execute(query)
					except MySQLdb.Error, e:
						print time.ctime(), ": Error en query en registrar el traspaso al superior", idUsuarioAplicar, " - Folio:", folio, e
						cursor.execute("ROLLBACK")
						return False

				# Actualizar el nuevo saldo del usuario en el array de saldos a aplicar.
				saldosUsuariosInterno[idUsuarioAplicar][columnSaldo] = nuevoSaldo
				nuevoSaldoNivelDos = nuevoSaldo

				if esServicio:
					break
			else:
				pass

		aplicarTotalCadena = round( ( depositado * (comisionNivelDos/100) ) + depositado, 2)
		comisionTotalPorcentual = round( (1 - (depositado/aplicarTotalCadena)) * 100, 2)

		# Actualización del depósito.
		query = """UPDATE depositos
		SET status = 3, aplicado_id_usuario = '%s', fechaaplicado = '%s', aplicar = '%s', niveldos='%s', totalaplicar = '%s'
		WHERE id_deposito = '%s'""" % (aplibotId, fechaAplicado, aplicar_1, nivelDos, aplicarTotalCadena, idDeposito)
		try:
			cursor.execute(query)
		except MySQLdb.Error, e:
			print time.ctime(), ": Error en query actualizar Depósito - Folio:", folio, e
			cursor.execute("ROLLBACK")
			return False

		# Registrar traspaso del deposito del usuario que lo realizó
		query = """
		INSERT INTO htraspasos (de,para,monto,fecha,comentarios,inicials,finals,inicialu,finalu,comision,ip,m,id_deposito,tipo)
		VALUES ('%s', '%s', '%s', '%s', 'Traspaso desde Sistema', '0', '0', '%s', '%s', '%s', '127.0.0.1', '0', '%s', '%s')
		""" % (padreInmediato_1, idUsuario, aplicar_1, fechaAplicado, saldo_1, nuevo_saldo_1, comisionPesos_1, idDeposito, deposito['servicio'])
		try:
			cursor.execute(query)
		except MySQLdb.Error, e:
			print time.ctime(), ": Error en query en registrar el traspaso al superior", idUsuarioAplicar, " - Folio:", folio, e
			cursor.execute("ROLLBACK")
			return False

		# Crear mensajes de notificación
		if portalAbrv == 'rec':
			cuerpoMail = "<b>Recargaki - Recarga Electrónica de Tiempo Aire</b>   <font color='blue'><b>Notificación de Aplicación de Saldo</b></font><br><br>"
			cuerpoMail += "Estimado Usuario <b>%s</b>,<br><br>" % razonCliente
			cuerpoMail += "Le informamos que su reporte de pago ha sido revisado exitosamente, de igual forma se ha aplicado el saldo correspondiente a su cuenta: <b>"+nombreUsuario+"</b>. <br><br>"

			cuerpoMail += "<b>Folio:</b> %s<br><br><br>" % folio
			cuerpoMail += "<b>Cantidad:</b> $%s<br><br><br>" % depositado
			cuerpoMail += "<b>Banco:</b> "+nombreBanco+"<br><br><br>"
			cuerpoMail += "<b>Fecha de Depósito:</b> "+fechaDep+"<br><br><br>"
			cuerpoMail += "<b>Fecha de Reporte:</b> "+fechaAlta+"<br><br><br>"
			cuerpoMail += "<b>Fecha de Aplicación:</b> "+fechaAplicado+"<br><br><br>"
			cuerpoMail += "Gracias por su Compra <br><br><br>"

			mailSubject = "Aplicación de Saldo Recargaki  - Recarga Electrónica de Tiempo Aire"
		else:
			cuerpoMail = "<font face='Arial'><b><div style='text-align: center'>PlanetaeMx</div></b> <br>"
			cuerpoMail += "Buen día estimado cliente <b>%s</b>,<br><br>" % razonCliente
			cuerpoMail += "Planetaemx le notifica que su saldo ya ha sido aplicado <b></b>. <br><br> Datos de su depósito:</font><br><br>"

			cuerpoMail +="<font face='Arial' size='-1'><b>Usuario:</b> "+nombreUsuario+" <br>"
			cuerpoMail +="<b>Entidad Bancaria:</b> "+nombreBanco+"<br>"
			cuerpoMail +="<b>Número de Autorización:</b> %s<br>" % folio
			cuerpoMail +="<b>Cantidad depositada:</b> $%s<br>" % depositado
			cuerpoMail +="<b>Fecha de Depósito:</b> "+fechaDep+"<br>"
			cuerpoMail +="<b>Saldo Aplicado:</b>$%s<br><br>" % aplicar_1
			cuerpoMail +="<font color='#005200'><u><b>Nuevo Saldo:</b> $%s</u></font></font><br><br>" % nuevo_saldo_1
			cuerpoMail +="<font face='Arial'>Agradeciendole por su compra se despide de usted Planetamx.</font> <br> <div style='text-align: center'>www.planetaemx.com</div><br><br>"

			mailSubject = "PlanetaeMx - Aut: %s  SALDO APLICADO" % folio

		mail = MIMEText(cuerpoMail, "html", "utf-8")
		mail['Subject'] = mailSubject
		mail['From'] = remitente
		mail['To'] = destinatario

		print time.ctime(), ": Depósito aplicado. Folio: "+ folio +" - Cliente: %s" % razonCliente
		# En caso de que haya sido exitoso, devuelve lo que se le debe de sumar al saldo del cliente y el mail a enviar
		resultado = {
			'saldosUsuarios': saldosUsuariosInterno,

			# En caso de querer enviar a varios destinatarios, enviarlos como Lista,
			# deben de estar también en el campo Bcc del mail
			'mail': {
				'mail': mail,
				'remitente': remitente,
				'destinatario': destinatario,
				'folio': folio
				}
			}
		return resultado
	# ------------------ Fin de la Función -------------------

	# --------- Obteción y aseguramiento de los datos a actualizar -------
	try:
		cursor.execute("START TRANSACTION;")
	except MySQLdb.Error, e:
		print time.ctime(), ": Error al iniciar la Transacción", e
		cursor.execute("ROLLBACK")
		cursor.close()
		db.close()
		raise SystemExit

	# Para Versión=1 [Meridian v1.0]
	query1 = """
	SELECT dep.id_deposito, dep.cantidad, dep.portal, dep.folio, dep.fecha, dep.fechaalta, dep.tipo,
               d_users.razon, d_users.id_usuario, d_users.email, d_users.nombre as nombreUsuario,
               sal.saldo, sal.saldo_srv,
               banco.nombre as nombreBanco, banco.tipo_servicio as servicio
	FROM depositos AS dep

	INNER JOIN datosusuarios as d_users ON d_users.id_usuario = dep.id_cliente
	INNER JOIN usuarios as users        ON users.id_usuario   = dep.id_cliente
	INNER JOIN saldos as sal            ON sal.id_usuario     = dep.id_cliente
	INNER JOIN bancos as banco          ON banco.id_banco     = dep.id_banco

	WHERE dep.status = 2 AND dep.id_banco IN (%s) AND users.eliminado = 0
	      AND dep.fecha > '0000-00-00'

	FOR UPDATE;
	""" % subqueryBancos

	# Selección del query según la Versión del Sistema
	query = {
		1: query1
		}[portalVersion]

	try:
		cursor.execute(query)
	except MySQLdb.Error, e:
		print time.ctime(), ": Error en query para obtener lista de depositos a actualizar", e
		cursor.close()
		db.close()
		raise SystemExit
	# ------------------- Datos obtenidos y asegurados -------------------

	# -------------------- Operaciones sobre los datos -------------------
	# Diccionario donde se guardarán y acumularán los saldos de cada usuario
	# hasta el final se actualizarán los saldos de los usuarios
	saldosUsuarios = {}
	# Lista de mails a enviar después de terminar todas las operaciones MySQL
	mailPool = []
	datosDepositos = cursor.fetchall()
	if len(datosDepositos) != 0:
		# Loop a través de los resultados
		for depositoData in datosDepositos:
			# Inicializar en saldosUsuarios el saldo del cliente si es que no existe en el diccionario
			userId = int( depositoData['id_usuario'] )
			if not saldosUsuarios.has_key(userId):
				if portalVersion == 1:
					saldosUsuarios[userId] = {}
					saldosUsuarios[userId]['saldo'] = float(depositoData['saldo'])
					saldosUsuarios[userId]['saldo_srv'] = float(depositoData['saldo_srv'])

			# Aplicar los cambios en la tabla de depositos según la versión
			# Y ejecutar función para hacer los cambios en la base de datos
			if portalVersion == 1:
				resultadoOp = operacionesDeposito1(depositoData, saldosUsuarios)

			if resultadoOp == False:
				brincar = True
				msjError += " Hubo un fallo en un depósito"
				break
			else:
				# Almacenar el mail que será enviado
				mailPool.append(resultadoOp['mail'])
				if portalVersion == 1:
					# Se reescribe el array de los saldos con los nuevos.
					saldosUsuarios = resultadoOp['saldosUsuarios']

		# Iterar por cada usuario en saldosUsuario para aplicarle su nuevo saldo
		for usuario, saldo in saldosUsuarios.iteritems():
			if portalVersion == 1:
				query = """
				UPDATE saldos
				SET saldo = '%s', saldo_srv = '%s'
				WHERE id_usuario = '%s';""" % (saldo['saldo'], saldo['saldo_srv'], usuario)
			try:
				cursor.execute(query)
			except MySQLdb.Error, e:
				print time.ctime(), ": Error en query Aplicar el nuevo saldo al Usuario:", usuario, e
				cursor.execute("ROLLBACK")
				brincar = True
				msjError += " Hubo fallo al actualizar saldos"
				break

	else:
		brincar = True
		msjError += " No hay depósitos que falten de aplicar saldo."

	# -------------- Fin de las operaciones sobre los datos --------------

	# -------------- Ejecutar Transacción y liberar recursos -------------
	# Realizar COMMIT para ejecutar la transacción y liberar las lineas bloqueadas
	if not brincar:
		try:
			cursor.execute("COMMIT;")
		except MySQLdb.Error, e:
			print time.ctime(), ": Error en COMMIT", e
			cursor.execute("ROLLBACK")
			cursor.close()
			db.close()
			raise SystemExit

		# ------------ Mandar mails informando sobre los depósitos -----------
		for email in mailPool:
			if not send_message( email['remitente'], email['destinatario'], email['mail'] ):
				print time.ctime(), ": Error al mandar el mail de aplicación al cliente - Folio:", email['folio']

# No se usa un Else, porque "brincar" puede ser puesto a True adentro del bloque anterior de código
if brincar:
	print time.ctime(), ":%s" % msjError
	cursor.close()
	db.close()
	raise SystemExit

####################################################
# Procedimientos de finalización

# Cerrar conexiones de la Base de Datos
cursor.close()
db.close()
