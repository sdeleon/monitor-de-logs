MONITOR
=======

Descripción
-----------

Revisa la validez de los registros de otro programa almacenados en un archivo de texto con cierto patrón en su nombre

Manda un email en el caso de encontrar una linea sin el patrón esperado o si hace falta un registro dentro del periodo esperado (uno cada 3 minutos).

Guarda las lineas con error de cada fecha en una base de datos Sqlite3.

No vuelve a enviar email en el caso de que no haya nuevas anomalías.


TODO
----

- Mandar todas las configuraciones al archivo de configuración (formato del nombre del archivo de registros, span de tiempo válido entre registros, etc)

- Limpiar el código, mejorar implementaciones.

- Mejorar este README


Requerimientos
--------------

* Archivo de configuración "monitor.cnf" según la sintaxis del paquete Configurator

        smtp_login
        {
            host = ""
            port = 25
            user = ""
            pass = ""
        }
        on_error_dest
        {
            from = ""
            to = ""
            subject = ""
        }

* Los siguientes paquetes de cabal

    time

    smtp-mail

    HDBC-sqlite3

    configurator
