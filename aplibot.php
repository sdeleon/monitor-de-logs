<?php
$debug = false;
$testMode = false;
$devMode = false;
$user = 'aplicador';
$workdir = $devMode ? '/home/ppc/www/recargaki': '/home/recargak/public_html';
$logDir = __DIR__.'/logs';
$logFile = $logDir.'/'.$user.'.'.date('Y-m-d').'.log';
$aplibotUserID = "4";
define('PORTAL_EMAILER', true);

ini_set('log_errors', 1);
ini_set('display_errors', 1);
ini_set('error_log', $logFile);
define('DEBUG_DEPPROCESSOR', $debug);
date_default_timezone_set('America/Mexico_City');
require_once $workdir.'/dpt/lib/depositProcessor.class.php';
require_once $workdir.'/conexiones/pdo.php';
require_once $workdir.'/config/solid.php';
require_once $workdir.'/lib/phpmailer/PHPMailerAutoload.php';

$now = function () {
    return date('Y-m-d H:i:s');
};

$db = &getDBConnection();

$res = $db->query(
    "SELECT permitidos FROM depousuarios WHERE id_usuario = ? AND permitidos > ''",
    ": Failed to get banks to check",
    array($aplibotUserID));

if (!$res || count($res) < 1) {
    error_log(": No hay bancos asignados al usuario del bot de aplicación de saldo");
    exit();
}

$banksList = substr($res[0]['permitidos'], 0, -1);

$res = $db->query(
    "SELECT dep.id_deposito
    FROM depositos AS dep
    INNER JOIN usuarios as users ON users.id_usuario = dep.id_cliente
    WHERE dep.status = 2 AND dep.id_banco IN ($banksList) AND users.eliminado = 0 AND dep.fecha > '0000-00-00'",
    ": Failed to get deposits to check");

if (!$res || count($res) < 1) {
    error_log(": No hay depósitos que falten de aplicar saldo.");
    exit();
}

foreach ($res as $depData) {
    $depositID = $depData['id_deposito'];

    $dp = new DepositProcessor($aplibotUserID);
    $result = $dp->applyDepositBalance($depositID);

    if (!$result['Success']) {
        die($result['Message']);
    }

    $res = $dp->getDepositData($depositID);
    $depositData = $res['Data'];
    $clientID = $depositData['ClientID'];

    $db = &getDBConnection();

    $res = $db->query(
        'SELECT finalu AS FinalBalance
        FROM htraspasos
        WHERE id_traspaso = ?',
        'dpt/status2: failed to get transaction data '.$result['Data']['TransactionID'],
        array($result['Data']['TransactionID']));
    $finalBalance = $res[0]['FinalBalance'];

    $res = $db->query(
        'SELECT a.nombre AS Username, a.razon AS CompanyName, a.email AS Email
        FROM datosusuarios AS a
        WHERE a.id_usuario=?',
        'dpt/status2: failed to get client data '.$clientID,
        array($clientID));
    $clientData = $res[0];

    $bankName = htmlentities($depositData["BankName"]);
    $authorization = htmlentities($depositData["Folio"]);

    $db->destroy();
    $db = null;

    if ($depositData['PortalType']=='Recargaki') {
        $htmlBody = "
        <b>Recargaki - Recarga Electrónica de Tiempo Aire</b>
        <font color='blue'><b>Notificación de Aplicación de Saldo</b></font><br><br>

        Estimado Usuario <b>" . $clientData['CompanyName']. "</b>,<br><br>
        Le informamos que su reporte de pago ha sido revisado exitosamente,
        de igual forma se ha aplicado el saldo correspondiente a su cuenta:
        <b>" . $clientData['Username']. "</b>. <br><br><br>

        <b>Folio:</b> ".$authorization."<br><br>
        <b>Cantidad:</b> $".$depositData['Amount']."<br><br>
        <b>Banco:</b> ".$bankName."<br><br>
        <b>Fecha de Depósito:</b> ".$depositData["DepositDate"]."<br><br>
        <b>Fecha de Reporte:</b> ".$depositData["ReportingDT"]->format('Y-m-d H:i:s')."<br><br>
        <b>Fecha de Aplicación:</b> ".$depositData["ApplicationDT"]->format('Y-m-d H:i:s')."<br><br><br>

        Gracias por su Compra <br><br><br>";
        $subject = "Aplicación de Saldo Recargaki  - Recarga Electrónica de Tiempo Aire ";
    } else { // Planetae Portal
        $htmlBody = "
        <font face='Arial'><b><div style='text-align: center'>PlanetaeMx</div></b><br>
        Buen día estimado cliente <b>" . $clientData['CompanyName']. "</b>,<br><br>
        Planetaemx le notifica que su saldo ya ha sido aplicado <b></b>. <br><br>
        Datos de su depósito:</font><br><br><br>

        <font face='Arial' size='-1'>
            <b>Usuario:</b> " . $clientData['Username']. " <br>
            <b>Entidad Bancaria:</b> ".$bankName."<br>
            <b>Número de Autorización:</b> ".$authorization."<br>
            <b>Cantidad depositada:</b> $".number_format($depositData['Amount'],2,".",",")."<br>
            <b>Fecha de Depósito:</b> ".htmlentities($depositData["DepositDate"])."<br>
            <b>Saldo Aplicado:</b>$".number_format($depositData['AmountApplied'],2,".",",")."<br><br>
            <font color='#005200'><u><b>Nuevo Saldo:</b> $".number_format($finalBalance,2,".",",")."</u></font>
        </font><br><br>

        <font face='Arial'>Agradeciendole por su compra se despide de usted Planetamx.</font> <br>
        <div style='text-align: center'>www.planetaemx.com</div><br><br>";
        $subject = "PlanetaeMx - Aut: ".$authorization."  SALDO APLICADO  ";
    }

    if ($debug) {
        error_log("\n$htmlBody\n");
    }

    $destination = $clientData["Email"];
    $senderData = $emailAccounts['Sender']['depositos@recargaki.com'];
    $hostData = $emailHosts['recargaki.com'];

    $mailer = new PHPMailer;
    $mailer->isSMTP();
    $mailer->CharSet = 'UTF-8';
    $mailer->Host = $hostData['Host'];
    $mailer->Port = $hostData['Port'];
    $mailer->SMTPSecure = $hostData['SMTPSecurity'];
    $mailer->SMTPOptions = $hostData['SMTPOptions'];
    $mailer->SMTPAuth = $hostData['SMTPAuth'];

    $mailer->Username = $senderData['Username'];
    $mailer->Password = $senderData['Password'];
    $mailer->setFrom($senderData['Email']);
    $mailer->addAddress($destination);
    $mailer->isHTML(true);

    $mailer->Subject = $subject;
    $mailer->Body = $htmlBody;
    if (!$testMode) {
        $res = $mailer->send();
    }
    error_log(": Depósito aplicado. Folio: ".$depositData['Folio'].
        " - Cliente: ".$clientData['CompanyName'].
        " | Email Res:".var_export($res, true));
    sleep(2);
}