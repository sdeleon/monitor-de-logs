{-# LANGUAGE OverloadedStrings #-}

{-
    Monitor v1.0-1
    Requires from cabal:
        time
        smtp-mail
        HDBC-sqlite3
        configurator
-}

import Sendmail
import MiscFunctions
import System.Locale
import System.IO
import Data.List
import Data.Time
import Data.Time.LocalTime
import Data.Time.Clock.POSIX
import Control.Applicative
import Database.HDBC 
import Database.HDBC.Sqlite3
import qualified Data.Configurator as Conf
import qualified Data.Configurator.Types as Types

type NumberedRows = [(Integer, String)]
type Rows = [String]
type Row = String
type ISODate = String

type DBRow_DateLines = (String, String)
type LineNumbers = [Integer]


getUTCTimeFromLine :: Row -> Maybe UTCTime
getUTCTimeFromLine [] = Nothing
getUTCTimeFromLine line
    | badPattern    = Nothing
    | otherwise     = utcTime
    where
        divided     = words line
        badPattern  = length divided < 5
        strDate     = unwords . take 5 $ divided
        utcTime     = parseTime defaultTimeLocale "%a %b %e %H:%M:%S %Y" strDate :: Maybe UTCTime

checkSanity :: [Maybe UTCTime] -> [Bool]
checkSanity []          = []
checkSanity [x]
    | x == Nothing      = [False] 
    | otherwise         = [True]
checkSanity (x:y:xs)
    | circaRightSpan    = True  : checkSanity (y:xs)
    | x == Nothing      = False : checkSanity (y:xs)
    | x == y            = True  : checkSanity (y:xs)
    | otherwise         = False : checkSanity (y:xs)
    where
        spanSecMinBound = Just (170 :: POSIXTime)
        spanSecMaxBound = Just (190 :: POSIXTime)
        posixTime1      = utcTimeToPOSIXSeconds <$> x
        posixTime2      = utcTimeToPOSIXSeconds <$> y
        secsDiff        = (-) <$> posixTime2 <*> posixTime1
        -- A (Nothing) will always be lower than any number so it will be ok to test it within a Range
        circaRightSpan  = spanSecMinBound <= secsDiff && secsDiff <= spanSecMaxBound

getOffensiveLines :: Rows -> NumberedRows
getOffensiveLines [] = []
getOffensiveLines rows = let 
    dateList        = map getUTCTimeFromLine rows
    sanityList      = checkSanity dateList
    numberedList    = zip3 [1..] rows sanityList
    filter (n, string, valid) stack = if valid 
                                        then stack
                                        else (n, string) : stack
    in foldr filter [] numberedList

offensiveLinesToDBRow :: ISODate -> NumberedRows -> Maybe DBRow_DateLines
offensiveLinesToDBRow _ []         = Nothing
offensiveLinesToDBRow date numberedList = let
    lineNumbers     = foldr (\row stack -> (show $ fst row):stack) [] numberedList
    lineNumString   = unwords lineNumbers
    in Just (date, lineNumString)

linesDiff :: NumberedRows -> LineNumbers -> NumberedRows
offensiveRows `linesDiff` retrievedLines = let
    filter oldLines row@(n, string) stack = if n `elem` oldLines
                                                then stack
                                                else row:stack
    in foldr (filter retrievedLines) [] offensiveRows

sendNotification :: NumberedRows -> IO ()
sendNotification newOffenders = do 
    cfg          <- Conf.load [Types.Required "monitor.cnf"]
    mailFrom     <- Conf.require cfg "on_error_dest.from" :: IO String
    mailTo       <- Conf.require cfg "on_error_dest.to" :: IO String
    mailSubject  <- Conf.require cfg "on_error_dest.subject" :: IO String
    let 
        allLines    = intercalate "<br />" $ [ show n ++ ": " ++ string | (n, string) <- newOffenders]
        htmlText    = unlines [
            "Nuevos errores",
            "<br /><br />",
            allLines,
            "<br /><br />",
            "Saludos y buena suerte :D"]
    mailSender ( mailFrom, mailTo, mailSubject, htmlText )

-- Database handling functions 

initMonitorDB :: IO Connection
initMonitorDB = do
    conn <- connectSqlite3 "aplimon.db"
    let 
        query = unlines [
            "CREATE TABLE IF NOT EXISTS ignore (",
            "date datetime PRIMARY KEY, ",
            "lines TEXT)"
            ]
    run conn query []
    commit conn
    return conn

getOldOffensiveLines :: Connection -> ISODate -> IO LineNumbers
getOldOffensiveLines conn date = do
    result      <- quickQuery' conn "SELECT lines FROM ignore WHERE date=?" [toSql date]
    let 
        extract []              = []
        extract [[retrieved]]   = fromSql retrieved :: String
        lines'                  = words . extract $ result
        lines                   = foldr (\n stack -> (read n):stack) [] lines'
    return lines

saveOffensiveLines :: Connection -> DBRow_DateLines -> IO Connection
saveOffensiveLines conn (date, lines) = do
    let 
        query = "INSERT OR REPLACE INTO ignore (date, lines) VALUES (?, ?)"
    run conn query [toSql date, toSql lines]
    commit conn
    return conn


main = do
    currentDateTime <- getISODateTimeNow
    let date            = take 10 currentDateTime
        currentDate     = date ++ " 00:00:00"
        filePath        = "logs/aplicador."++date++".log"
    contents <- readFile filePath
    conn <- initMonitorDB
    linesToIgnore <- getOldOffensiveLines conn currentDate
    let rows            = lines contents
        offensiveLines  = getOffensiveLines rows
        dbRow           = offensiveLinesToDBRow currentDate offensiveLines
        newOffenders    = offensiveLines `linesDiff` linesToIgnore
    if null newOffenders 
        then putStr "No se encontraron errores\n"
        else do 
            let (Just linesToSave) = dbRow
            saveOffensiveLines conn linesToSave
            sendNotification newOffenders
            putStr "Nuevos errores encontrados enviando:\n"
            putStr . show $ newOffenders
            putStr "\n"
    disconnect conn